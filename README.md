Scientific presentations
===================================

Author: _Wouter A.A. de Steenhuijsen Piters_.

This repository shows an overview of my oral scientific presentations to date. For an overview of scientific posters please see [here](https://gitlab.com/wsteenhu/posters).
Only lectures and presentations at external conferences are shown.

## Overview

Year | Event | Location | Title | Invited
--- | --- | --- | --- | ---
2023 | Lecture FOS course ‘Cutting-edge immunology: from chemistry to metabolism’ | LUMC, Leiden, NL | Respiratory tract microbiota and infection susceptibility | ✔️
2023 | Lecture minor 'Infectious Diseases and Health' | LUMC, Leiden, NL | Respiratory microbiota in early life. Impact on respiratory infection. | ✔️
2023 | Lecture minor 'Infectious Diseases and Health' | LUMC, Leiden, NL | The human microbiome - introduction | ✔️
2022 | European Respiratory Society Course entitled [Virtual Academy on Lung Microbiome: from Study Design to Data Analysis](https://www.ersnet.org/wp-content/uploads/2022/06/Lung-Microbiome-Programme-final-vf-oct-22.pdf) | Online | Tools for data analysis (including case presentation) | ✔️
2022 | [Exploring Human Host-Microbiome Interactions in Health and Disease](https://coursesandconferences.wellcomeconnectingscience.org/wp-content/uploads/2021/11/Microbiome-Interactions-in-Health-and-Disease-2022-programme.pdf) | Hinxton, United Kingdom | The upper respiratory tract microbiota across the human lifespan: a nationwide Dutch population study<sup>1</sup> | -
2022 | Lecture minor 'Infectious Diseases and Health' | LUMC, Leiden, NL | Respiratory microbiota in early life | ✔️
2022 | [12th International Symposium on Pneumococci and Pneumococcal Diseases (ISPPD-12)](https://isppd.kenes.com) | Toronto, Canada | Meet-the-Expert Session 07: The Microbiome and Relevance to Pneumococcal Disease | ✔️
2021 | Lecture minor 'Infectious Diseases and Health' | LUMC, Leiden, NL | Respiratory microbiota in early life | ✔️
2020 | Lecture minor 'Infectious Diseases and Health' | LUMC, Leiden, NL | Respiratory microbiota in early life - Impact on mild respiratory infections | ✔️
2019 | European Respiratory Society Research Seminar "Early origins of lung disease: towards an interdisciplinary approach" | Lisbon, Portugal | Early life respiratory infections: diagnosis, prognosis and treatment - Impact of the upper respiratory tract microbiota<sup>2</sup> | ✔️
2019 | [Exploring Human Host-Microbiome Interactions in Health and Disease](https://coursesandconferences.wellcomegenomecampus.org/our-events/exploring-human-host-microbiome-interactions-2019/) | Hinxton, United Kingdom | Interaction between the nasal microbiota and _S. pneumoniae_ in the context of live-attenuated influenza vaccine (LAIV) | -
2018 | Masterclass WEON2018 Dutch Epidemiological Conference | RIVM, Bilthoven, NL | Exposome and microbiome: opportunities and challenges Part 2 – Early life microbiota development and statistical challenges | ✔️
2018 | [World Pneumonia Day Symposium Edinburgh](https://www.ed.ac.uk/inflammation-research/news-events/2018/world-pneumonia-day-symposium) | UoE, Edinburgh, UK | The local microbiota in respiratory tract infection | ✔️
2017 | [International Scientific Symposium on Healthy Ageing](https://www.longevitylille.fr/index.php?page=edition-2017) | Institut Pasteur de Lille, Lille, France | Age-related changes in the respiratory microbiota in relation to respiratory health and disease | ✔️
2017 | Symposium Top Institute Food and Nutrition - Maintaining Oral Health | [ACTA](https://www.acta.nl/en/index.aspx), Amsterdam, NL | Maturation of the respiratory tract microbiota in relation to respiratory health | ✔️
2016 | [Exploring Human Host-Microbiome Interactions in Health and Disease](https://coursesandconferences.wellcomegenomecampus.org/our-events/exploring-human-host-microbiome-interactions-in-health-and-disease-2016/) | Hinxton, United Kingdom | Association between maturation of respiratory microbiota and risk of respiratory infections in infants | -
2016 | [European Congress of Clinical Microbiology & Infectious Diseases (ECCMID)](https://2016.eccmid.org) | Amsterdam, NL | Nasopharyngeal microbiota, host transcriptome and disease severity in children with RSV infection<sup>3</sup> | - 
2014 | Masterclass Anatomy Lesson 2014 - Prof. dr. Martin Blaser | AMC, Amsterdam, NL | Dysbiosis of upper respiratory tract microbiota in elderly pneumonia patients | -
2014 | Exploring Human Host-Microbiome Interactions in Health and Disease | Hinxton, United Kingdom | Dysbiosis of upper respiratory tract microbiota in elderly pneumonia patients | -
2014 | [Scientific Spring Meeting KNVM & NVMM](https://s3-eu-west-1.amazonaws.com/congressus-knvm/files/7a2f30d593764edaaf6948bc881d2335.pdf) | Papendal, NL | Dysbiosis of upper respiratory tract microbiota in elderly pneumonia patients* | - 
2014 | Current Topics in Infectious Diseases | Grindelwald, Switzerland | Infection in oncology. Case report and interactive discussion. | ✔️ 

<sup>1</sup> presented by Mari-Lee Odendaal (PhD-student) under my supervision.  
<sup>2</sup> Following discussions at this symposium, I co-authored a review on the early origins of lung disease, published in [_European Respiratory Review_](https://err.ersjournals.com/content/29/157/200191).   
<sup>3</sup> presented by [Debby Bogaert](https://www.ed.ac.uk/inflammation-research/people/principal-investigators/professor-debby-bogaert) <a href="https://orcid.org/0000-0003-2651-7000" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>.  

## License

All presentations are licensed with [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/).